#!/usr/bin/python

##
# ctopython.py
# 
# This is a python script file. This file should use in python version 2.*
# only. This is an important point to note. 
# 
# This file has open sourec lincese. Feel free to use it in your own projects.
# 
# @package C2Python
# @author rajeevktomy <rajeev.rktthestar@gmail.com>
# @version Python2.7
##

##
# c2Python Script
# 
# This python file is actually an experiment  try for converting a c program to
# a python script.
##

# initialize variables
# python does not require a semicolon at the end. 
flag = set = i = j = n = 0
ar = [] #declaring a list; in python list is equivalent to array

# use to do a command prompt; 
# this way you can get and store inputs in python
tkno = int(raw_input("enter the first token number\n"))
lstno = int(raw_input("enter todays last token number\n"))
n = lstno - tkno

#Python do not use parantheses in conditions. Instead it uses ":"
#Python gives importance to "block"ing
#It requires that we need to put same indentation to all statements which 
#comes under a block. statements inside this "while" statement is a block.
while (tkno <= lstno) : 
    print tkno
    flag = int(raw_input("\nif person is absent press 1 else press 0\n"))

    if (flag == 1) :
		ar.append(tkno) #stores a value to the last location of a list
		i += 1 		#no i++ in python.
		flag = 0
		print "sorry\n"
		n +=1
    
    else :
		print "on process\n"

    #There is no label/goto statement in python
    # In any language, you should avoid label/goto since it consider as bad code
	# speed of execution reduces due to those "random jumpings"
    set = int(raw_input("if the process is over for token number then press 1 else press 0\n"))

    if (set == 1) :
        tkno = tkno+1

    set = 0
    n -= 1

if(n >= 0) :
	print "second call \n"
	while (j <= i-1) :
		print ar[j]
		set =  int(raw_input("\nif process is over for next token then press 1 else press 0\n"))

		if (set == 1) :
			j += 1
	set = 0
	print "thank you\n"