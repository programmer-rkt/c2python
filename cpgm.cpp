/**
 * cpgm.cpp
 *
 * This is a sample c++ program script file
 *
 * This file has open sourece license
 *
 * @package C2Python
 * @author Rajeev K Tomy <rajeevphpdeveloper@gmail.com>
 */

/**
 * this is a sample c++ program
 */
#include<iostream.h>
#include<conio.h>
void main()
{
	clrscr();
	int flag, set, i, j, tkno, lstno, ar[50], n;
	flag = set = i = j = 0;
	cout << "enter the first token number\n";
	cin >> tkno;
	cout << "enter todays last token number\n";
	cin >> lstno;
	n = lstno - tkno;
	while (tkno <= lstno)
	{
		cout << tkno << "\n";
		cout << "if person is absent press 1 else press 0\n";
		cin >> flag;
		if (flag == 1)
		{
			ar[i] = tkno;
			i++;
			flag = 0;
			cout << "sorry\n";
			n++;
			goto T;
		}
		cout << "on process\n";
		T:cout << "if the process is over for token number then press 1 else press 0\n";
		cin >> set;
		if (set == 1)
			tkno = tkno+1;
		set = 0;
		n--;
	}
	if(n >= 0)
	{
		cout << "second call \n";
		while (j <= i-1)
		{
			cout << ar[j];
			cout << "if process is over for next token then press 1 else press 0\n";
			cin >> set;
			if (set == 1)
				j++;
		}
		cout << "thank you\n";
		set = 0;
	}
	getch();
}